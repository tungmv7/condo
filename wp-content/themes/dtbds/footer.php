
<footer class="footer1">
    <div class="container">
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 first clearfix">
                <div class="widget clearfix">
                    <div class="title"><h3><i class="fa fa-home"></i>Company info</h3>
                        <hr>
                    </div>
                    <ul class="list">
                        <li><a title="" href="/about-us">About us</a></li>
                        <li><a title="" href="/contact-us">Contact us</a></li>
                        <li><a title="" href="/legal">Legal</a></li>
                        <li><a title="" href="/support">Support</a></li>
                        <li><a title="" href="/search">Properties</a></li>
                    </ul>
                </div>
            </div><!-- end col-lg-3 -->
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 col-xs-12 clearfix">
                <div class="widget clearfix">
                    <div class="title"><h3>Popular location</h3>
                        <hr>
                    </div>
                    <ul class="list">
                        <?php
                        foreach (get_terms("project-area") as $location) {
                            $term_link = get_term_link($location);
                            echo '<li><a href="' . esc_url($term_link) . '">' . $location->name . '</a></li>';
                        }
                        ?>
                    </ul>
                </div>
            </div><!-- end col-lg-3 -->
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 col-xs-12 clearfix">
                <div class="widget clearfix">
                    <div class="title"><h3>properties</h3>
                        <hr>
                    </div>
                    <ul class="list">
                        <?php
                        foreach (get_terms("project-type") as $location) {
                            $term_link = get_term_link($location);
                            echo '<li><a href="' . esc_url($term_link) . '">' . $location->name . '</a></li>';
                        }
                        ?>
                    </ul>
                </div>
            </div><!-- end col-lg-3 -->
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 last clearfix" style="background-color: #212121;">

                <div class="widget clearfix">
                    <div class="title"><h3>Connect With Us</h3>
                        <hr>
                    </div>
                    <div class="social clearfix pull-right">
                        <span><a data-placement="top"
                                 data-toggle="tooltip"
                                 data-original-title="Facebook" title=""
                                 href="https://www.facebook.com/Condo/"><i
                                        class="fa fa-facebook" style="color: #ffffff;"></i></a></span>
                        <span><a data-placement="top"
                                 data-toggle="tooltip"
                                 data-original-title="Twitter" title=""
                                 href="https://twitter.com/propertyvn/"><i
                                        class="fa fa-twitter" style="color: #ffffff;"></i></a></span>
                        <span><a data-placement="top"
                                 data-toggle="tooltip"
                                 data-original-title="Linkedin " title=""
                                 href="https://www.linkedin.com/in/propertyvn/"><i
                                        class="fa fa-linkedin " style="color: #ffffff;"></i></a></span>
                        <span><a data-placement="top"
                                 data-toggle="tooltip"
                                 data-original-title="Google Plus"
                                 title=""
                                 href="https://plus.google.com/u/0/113239429575134296689"><i
                                        class="fa fa-google-plus" style="color: #ffffff;"></i></a></span>

                    </div>
                    <!-- <div class="title"><h3><i class="fa fa-envelope-o"></i> Newsletter Form</h3><hr></div> -->

                    <p>Subscribe to our Newsletter to recevie money-saving deals and condo market
                        news</p>
                    <form class="form-inline" role="form">
                        <div class="form-group">
                            <input type="email" class="form-control" id="exampleInputEmail2" placeholder="Enter email">
                        </div>
                        <button type="submit" class="btn btn-primary" style="border-radius: 0;">SUBSCRIBE</button>
                    </form>

                </div>
            </div><!-- end col-lg-4 -->
        </div><!-- row -->
    </div><!-- container -->
</footer><!-- footer1 -->
<?php $contactData = wp_cache_get('contact-data'); ?>
<section class="copyright">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p style="text-align: center">
                    <small> © Copyright <?= date("Y") ?> PropertyVN.org, All rights reserved.</small>
                </p>
            </div>
        </div><!-- end row -->
    </div><!-- end container -->
</section><!-- end copyright -->

<?= wp_footer() ?>
</body>
</html>
