<?php
/* Template Name: Home Template */
get_header();
?>

    <style>
        .searchmodule .btn-search-main {
            height: auto;
            line-height: 34px;
            margin-top: 12px;
            font-size: 18px;
            background-image: linear-gradient(to right,#976f40 1%,#fecb85 56%,#976f40 100%);
            border: 0;
            color: #033;
            padding: 6px 12px;
            display: block;
            width: 100%;
            -webkit-box-shadow: inset 0 1px 0 rgba(255,255,255,.5), 0 1px 2px rgba(0,0,0,.25);
            box-shadow: inset 0 1px 0 rgba(255,255,255,.5), 0 1px 2px rgba(0,0,0,.25);
        }
        .slides .ps-desc {
            left: 0;
            right: 0;
            top: auto;
            bottom: 0;
            border-radius: 0;
            width: 100%;
            padding: 10px;
        }
        .slides .ps-desc h3,
        .slides .ps-desc h4 {
            text-align: center;
        }
    </style>

    <!-- Slide trang chủ -->
    <section id="one-parallax" class="parallax"
             style="background-image: url('/wp-content/themes/dtbds/images/background.jpg');"
             data-stellar-background-ratio="0.6" data-stellar-vertical-offset="20">
        <div class="mapandslider">
            <div class="overlay1 dm-shadow">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-12 main-custom-slide-container">
                            <div id="property-slider" class="clearfix">
                                <div class="flexslider">
                                    <ul class="slides">
                                        <?php
                                        $projects = getNews(5, 1, [
                                            'tax_query' => [
                                                [
                                                    'taxonomy' => 'category',
                                                    'field' => 'slug',
                                                    'terms' => 'featured'
                                                ]
                                            ]
                                        ]);
                                        if ($projects) {
                                            while ($projects->have_posts()) {
                                                $projects->the_post();
                                                get_template_part('template-parts/project', 'item-7');
                                            }
                                        }
                                        ?>
                                    </ul><!-- end slides -->
                                </div><!-- end flexslider -->
                            </div><!-- end property-slider -->
                        </div><!-- end col-lg-8 -->
                        <div class="col-lg-4 col-md-4 col-sm-12">
                            <div class="searchmodule clearfix effect-slide-right in" data-effect="slide-right"
                                 style="transition: all 0.7s ease-in-out;">
                                <form id="advanced_search_module"
                                      action="<?= pll_current_language() == 'en' ? pll_home_url() . 'search' : pll_home_url() . 'tim-kiem' ?>"
                                      class="clearfix" name="advanced_search" method="get">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <label for="country" class="search-label" style="display: block !important;">Country</label>
                                        <select id="country" class="show-menu-arrow selectpicker"
                                                style="display: none;">
                                            <option value="vietnam">Viet Nam</option>
                                            <option value="malaysia">Malaysia</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <label for="location" class="search-label"
                                               style="display: block !important;"><?= pll__("Location") ?></label>
                                        <select id="advanced_search_module_location"
                                                class="show-menu-arrow selectpicker" style="display: none;"
                                                name="location">
                                            <option value="all"><?= pll__("All") ?></option>
                                            <!-- <?php
                                            foreach (get_terms("project-area", "order=DESC") as $location) {

                                                $selected = $formAttributes['location'] == $location->slug ? "selected" : "";
                                                echo "<option value='" . $location->slug . "' $selected>" . $location->name . "</option>";
                                            }
                                            ?> -->
                                            <option value="1">Ho Chi Minh City</option>
                                            <option value="2">Danang - Hoi An</option>
                                            <option value="3">Nha Trang</option>
                                            <option value="4">Phu Quoc Island</option>
                                            <option value="5">Ha Long Bay</option>
                                            <option value="6">Phan Thiet</option>
                                            <option value="7">Vung Tau</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <label for="status" class="search-label"
                                               style="display: block !important;"><?= pll__("Status") ?></label>
                                        <select id="advanced_search_module_status" class="show-menu-arrow selectpicker"
                                                style="display: none;" name="status">
                                            <option value="all"><?= pll__("All") ?></option>
                                            <?php
                                            foreach (get_terms("project-status", ["order" => 'DESC', 'lang' => pll_current_language()]) as $status) {
                                                $selected = $formAttributes['status'] == $status->slug ? "selected" : "";
                                                echo "<option value='" . $status->slug . "' $selected>" . $status->name . "</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <label for="lptype" class="search-label"
                                               style="display: block !important;"><?= pll__("Type") ?></label>
                                        <select id="lptype" class="show-menu-arrow selectpicker" style="display: none;"
                                                name="type">
                                            <option value="all"><?= pll__("All") ?></option>
                                            <?php
                                            foreach (get_terms("project-type", ["order" => 'DESC', 'lang' => pll_current_language()]) as $type) {
                                                $selected = $formAttributes['type'] == $type->slug ? "selected" : "";
                                                echo "<option value='" . $type->slug . "' $selected>" . $type->name . "</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <label for="bedrooms" class="search-label" style="display: block !important;">Bedrooms</label>
                                        <select id="bedrooms" class="show-menu-arrow selectpicker"
                                                style="display: none;">
                                            <option value="1">All</option>
                                            <option value="2">1</option>
                                            <option value="3">2</option>
                                            <option value="4">3</option>
                                            <option value="5">4</option>
                                            <option value="6">5</option>
                                            <option value="7">6</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <label for="baths" class="search-label"
                                               style="display: block !important;">Baths</label>
                                        <select id="baths" class="show-menu-arrow selectpicker" style="display: none;">
                                            <option value="1">All</option>
                                            <option value="2">1</option>
                                            <option value="3">2</option>
                                            <option value="4">3</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <label for="min_price" class="search-label" style="display: block !important;">Min
                                            Price</label>
                                        <select id="min_price" class="show-menu-arrow selectpicker"
                                                style="display: none;">
                                            <option value="0">$0</option>
                                            <option value="1000">$1000</option>
                                            <option value="5000">$5000</option>
                                            <option value="10000">$10000</option>
                                            <option value="25000">$25000</option>
                                            <option value="50000+">$50000+</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <label for="max_price" class="search-label" style="display: block !important;">Max
                                            Price</label>
                                        <select id="max_price" class="show-menu-arrow selectpicker"
                                                style="display: none;">
                                            <option value="1000">$1000</option>
                                            <option value="5000">$5000</option>
                                            <option value="15000">$15000</option>
                                            <option value="25000">$25000</option>
                                            <option value="50000">$50000</option>
                                            <option value="100000+">$100000+</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <label for="condition" class="search-label"
                                               style="display: block !important;"><?= pll__("Property Condition") ?></label>
                                        <select id="condition" class="show-menu-arrow selectpicker"
                                                style="display: none;" name="condition">

                                            <!--     <option value="all"><?= pll__("All") ?></option>
                                            <?php
                                            foreach (get_terms("project-area", "order=DESC") as $location) {
                                                $selected = $formAttributes['location'] == $location->slug ? "selected" : "";
                                                echo "<option value='" . $location->slug . "' $selected>" . $location->name . "</option>";
                                            }
                                            ?> -->

                                            <option value="1">New</option>
                                            <option value="2">Used - Like New</option>
                                            <option value="3">Used - Very Good</option>
                                            <option value="4">Used - Good</option>
                                            <option value="5">Used - Acceptable</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <label for="from_the" class="search-label" style="display: block !important;">From
                                            the</label>
                                        <select id="from_the" class="show-menu-arrow selectpicker"
                                                style="display: none;">
                                            <option value="agent">Agents</option>
                                            <option value="agencie">Agencies</option>
                                            <option value="property_owner">Property Owner</option>
                                            <option value="any">Any</option>
                                        </select>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <input type="submit" class="btn-search-main"
                                               value="<?= pll__("SEARCH PROPERTY") ?>">
                                    </div>
                                </form>
                            </div><!-- end search module -->
                        </div><!-- end col-lg-4 -->
                    </div><!-- end row -->

                </div><!-- end dm_container -->
            </div>
        </div>

    </section><!-- end mapandslider -->

    <section id="three-parallax" class="parallax"
             style="background-image: url(&#39;/wp-content/themes/dtbds/demos/02_home.jpg&#39;); opacity: 0.9;"
             data-stellar-background-ratio="0.6" data-stellar-vertical-offset="20">
        <div class="threewrapper">
            <div class="overlay1 dm-shadow">
                <div class="container">
                    <div class="row">
                        <div class="text-center clearfix">
                            <h3 class="big_title">Top Properties</h3>
                        </div>
                        <?php
                        $projects = getProjects(4);
                        if ($projects) {
                            while ($projects->have_posts()) {
                                $projects->the_post();
                                get_template_part('template-parts/project', 'item-3');
                            }
                        }
                        ?>
                    </div><!-- end row -->
                </div><!-- end container -->
            </div><!-- end overlay1 -->
        </div><!-- end threewrapper -->
    </section><!-- end parallax -->

    <section class="generalwrapper dm-shadow clearfix">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12 first clearfix">
                    <?= get_template_part('template-parts/project', 'categories') ?>
                </div>

                <div class="col-lg-7 col-md-9 col-sm-9 col-xs-12 clearfix">
                    <div id="tabbed_widget" class="tabbable clearfix effect-slide-bottom in" data-effect="slide-bottom"
                         style="transition: all 0.7s ease-in-out;">
                        <?php
                        $areas = get_terms("project-area", ['orderby' => 'count', 'limit' => 9, 'order' => 'DESC']);
                        if (!empty($areas)):
                            ?>
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#prj-location-all"><?= pll__("All") ?></a>
                                </li>
                                <?php
                                foreach ($areas as $k => $area) {
                                    echo "<li><a data-toggle=\"tab\" href='#prj-location-" . $area->slug . "'>" . $area->name . "</a></li>";
                                }
                                ?>
                            </ul>
                        <?php endif; ?>
                        <div class="tab-content tabbed_widget clearfix">
                            <div class="tab-pane row active" id="prj-location-all">
                                <?php
                                $projects = getProjects(12);
                                if ($projects) {
                                    while ($projects->have_posts()) {
                                        $projects->the_post();
                                        get_template_part('template-parts/project', 'item-2');
                                    }
                                }
                                ?>
                            </div>

                            <?php
                            foreach ($areas as $area):
                                ?>
                                <div class="tab-pane row" id="prj-location-<?= $area->slug ?>">
                                    <?php
                                    $projects = getProjects(9, 1, [
                                        'tax_query' => [
                                            [
                                                'taxonomy' => 'project-area',
                                                'field' => 'slug',
                                                'terms' => $area->slug
                                            ]
                                        ]
                                    ]);
                                    if ($projects) {
                                        while ($projects->have_posts()) {
                                            $projects->the_post();
                                            get_template_part('template-parts/project', 'item-2');
                                        }
                                    }
                                    ?>
                                </div>
                                <?php
                            endforeach;
                            ?>
                        </div>
                    </div><!-- tabbed widget -->
                    <div class="message message-section">
                        <a class="btn btn-primary btn-block btn-lg" href="/search/"><i class="fa fa-home"></i> VIEW ALL
                            PROPERTIES</a>
                    </div>
                </div> <!-- end col-lg-7 -->

                <div class="col-lg-3 col-md-6 col-sm-9 col-xs-12 last clearfix">
                    <?= get_template_part('template-parts/search', 'box') ?>
                    <?= get_template_part('template-parts/contact', 'box') ?>
                    <?php dynamic_sidebar('ads-homepage-2') ?>
                </div><!-- end col-lg-4 -->
            </div><!-- end dm_container -->
    </section><!-- end generalwrapper -->

    <!--    <section class="message_banner dm-shadow">-->
    <!--        <div class="container">-->
    <!--            <div class="row">-->
    <!--                <div class="col-lg-9 col-md-9 col-sm-12">-->
    <!--                    <div class="message">-->
    <!--                        <h2><strong>TIN & TIN</strong> REAL ESTATE</h2>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">-->
    <!--                    <div class="message message-section">-->
    <!--                        <a class="btn btn-primary btn-block btn-lg" href="/search/"><i class="fa fa-home"></i> VIEW ALL-->
    <!--                            PROPERTIES</a>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </section>-->

    <section id="two-parallax" class="parallax"
             style="background-image: url(&#39;/wp-content/themes/dtbds/demos/02_home.jpg&#39;);"
             data-stellar-background-ratio="0.6" data-stellar-vertical-offset="20">
        <div class="threewrapper">
            <div class="overlay1 dm-shadow">
                <div class="container">

                    <div class="row">
                        <div class="text-center clearfix">
                            <h3 class="big_title">Latest News</h3>
                        </div><!-- text-center -->

                        <?php $the_query = new WP_Query('posts_per_page=3', '&orderby=date&order=DESC'); ?>
                        <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>

                            <?php
                            $thumbnail = '/wp-content/themes/dtbds/images/default-thumb.png';
                            if ($url = get_the_post_thumbnail_url(get_the_ID(), 'medium')) {
                                $thumbnail = $url;
                            }
                            ?>

                            <!-- Tin tuc -->
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <article class="blog-wrap">
                                    <div class="ImageWrapper blog-media">
                                        <img class="img-responsive" src="<?= $thumbnail ?>" alt="" style="height: 250px;">
                                        <div class="ImageOverlayH"></div>
                                        <div class="Buttons StyleMg">
                                            <span class="WhiteSquare">
                                                <a class="fancybox" href="<?php echo $thumbnail ?>"><i
                                                            class="fa fa-search"></i></a></span>
                                            <span class="WhiteSquare"><a href="<?php the_permalink(); ?>"><i
                                                            class="fa fa-link"></i></a></span>
                                        </div>
                                    </div><!-- end blog media -->
                                    <div class="post-date">
                                        <span class="day"><?php the_time('jS'); ?></span>
                                        <span class="month"><?php the_time('F'); ?></span>
                                    </div><!-- end post-date -->

                                    <div class="post-content">
                                        <h2><a href="<?php the_permalink(); ?>" "=""><?php the_title(); ?></a></h2>
                                        <p><?php the_excerpt(); ?></p>
                                        <div class="post-meta">
                                            <span><i class="fa fa-user"></i> <a
                                                        href="javascript:;"><?php the_author(); ?></a></span>
                                            <span><i class="fa fa-comments"></i> <a
                                                        href="javascript:;"><?php comments_number('0', '1', '%'); ?>
                                                    Comments</a></span>
                                        </div>
                                        <a href="<?php the_permalink(); ?>" class="btn btn-primary btn-xs">xem tiếp</a>
                                    </div><!-- post-content -->

                                </article><!-- end blog wrap -->
                            </div>
                            <!-- end tin tuc -->

                            <?php
                        endwhile;
                        wp_reset_postdata();
                        ?>

                    </div><!-- end row -->

                </div><!-- end container -->
            </div><!-- end overlay1 -->
        </div><!-- end threewrapper -->
    </section><!-- end parallax -->

<?php get_footer(); ?>