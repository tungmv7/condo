<!-- blogs on homepage-->
<section class="secondwrapper dm-shadow clearfix">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-12">
                <h3 class="big_title">Recent Properties
                    <small>Handpicked Properties for you</small>
                </h3>
                <div id="slider" class="flexslider">
                    <ul class="slides">
                        <li class=""
                            style="width: 100%; float: left; margin-right: -100%; position: relative; display: none;">
                            <div class="ps-mini-desc">
                                <span class="type">Family Home</span>
                                <span class="price">€1 112 000</span>
                                <a href="/" class="status">On Sale</a>
                            </div>
                            <img class="img-thumbnail" src="/wp-content/themes/dtbds/raw/01_home.jpg" alt="">
                        </li>
                        <li style="width: 100%; float: left; margin-right: -100%; position: relative; display: none;"
                            class="">
                            <div class="ps-mini-desc">
                                <span class="type">Villa</span>
                                <span class="price">€3 111 000</span>
                                <a href="/" class="status">On Sale</a>
                            </div>
                            <img class="img-thumbnail" src="/wp-content/themes/dtbds/raw/02_home.jpg" alt="">
                        </li>
                        <li style="width: 100%; float: left; margin-right: -100%; position: relative; display: none;"
                            class="">
                            <div class="ps-mini-desc">
                                <span class="type">Apertment</span>
                                <span class="price">$2 000 000</span>
                                <a href="/" class="status">On Sale</a>
                            </div>
                            <img class="img-thumbnail" src="/wp-content/themes/dtbds/raw/03_home.jpg" alt="">
                        </li>
                        <li style="width: 100%; float: left; margin-right: -100%; position: relative; display: none;"
                            class="">
                            <div class="ps-mini-desc">
                                <span class="type">Family Home</span>
                                <span class="price">€1 112 000</span>
                                <a href="/" class="status">On Sale</a>
                            </div>
                            <img class="img-thumbnail" src="/wp-content/themes/dtbds/raw/04_home.jpg" alt="">
                        </li>
                        <li style="width: 100%; float: left; margin-right: -100%; position: relative; display: none;"
                            class="">
                            <div class="ps-mini-desc">
                                <span class="type">Triplex</span>
                                <span class="price">€3 100 000</span>
                                <a href="/" class="status">On Sale</a>
                            </div>
                            <img class="img-thumbnail" src="/wp-content/themes/dtbds/raw/05_home.png" alt="">
                        </li>
                        <li style="width: 100%; float: left; margin-right: -100%; position: relative; display: none;"
                            class="">
                            <div class="ps-mini-desc">
                                <span class="type">Family Home</span>
                                <span class="price">€4 211 000</span>
                                <a href="/" class="status">On Sale</a>
                            </div>
                            <img class="img-thumbnail" src="/wp-content/themes/dtbds/raw/06_home.png" alt="">
                        </li>
                        <li style="width: 100%; float: left; margin-right: -100%; position: relative; display: none;"
                            class="">
                            <div class="ps-mini-desc">
                                <span class="type">Home</span>
                                <span class="price">€3 000</span>
                                <a href="/" class="status">On Rent</a>
                            </div>
                            <img class="img-thumbnail" src="/wp-content/themes/dtbds/raw/07_home.png" alt="">
                        </li>
                        <li style="width: 100%; float: left; margin-right: -100%; position: relative; display: list-item;"
                            class="flex-active-slide">
                            <div class="ps-mini-desc">
                                <span class="type">Apertment</span>
                                <span class="price">€1 112 000</span>
                                <a href="/" class="status">On Sale</a>
                            </div>
                            <img class="img-thumbnail" src="/wp-content/themes/dtbds/raw/08_home.png" alt="">
                        </li>
                    </ul>
                    <ul class="flex-direction-nav">
                        <li><a class="flex-prev" href="http://designingmedia.com/html/estate-plus/index.html#">Previous</a>
                        </li>
                        <li><a class="flex-next flex-disabled"
                               href="http://designingmedia.com/html/estate-plus/index.html#">Next</a></li>
                    </ul>
                </div>
                <div id="carousel" class="flexslider">

                    <div class="flex-viewport" style="overflow: hidden; position: relative;">
                        <ul class="slides"
                            style="width: 1600%; transition-duration: 0s; transform: translate3d(-454px, 0px, 0px);">
                            <li class="flex-active-slide" style="float: left; display: block; width: 114px;"><img
                                    class="img-thumbnail" src="/wp-content/themes/dtbds/raw/01_home.jpg" alt="">
                            </li>
                            <li style="float: left; display: block; width: 114px;"><img class="img-thumbnail"
                                                                                        src="/wp-content/themes/dtbds/raw/02_home.jpg"
                                                                                        alt=""></li>
                            <li style="float: left; display: block; width: 114px;"><img class="img-thumbnail"
                                                                                        src="/wp-content/themes/dtbds/raw/03_home.jpg"
                                                                                        alt=""></li>
                            <li style="float: left; display: block; width: 114px;"><img class="img-thumbnail"
                                                                                        src="/wp-content/themes/dtbds/raw/04_home.jpg"
                                                                                        alt=""></li>
                            <li style="float: left; display: block; width: 114px;"><img class="img-thumbnail"
                                                                                        src="/wp-content/themes/dtbds/raw/05_home.png"
                                                                                        alt=""></li>
                            <li style="float: left; display: block; width: 114px;"><img class="img-thumbnail"
                                                                                        src="/wp-content/themes/dtbds/raw/06_home.png"
                                                                                        alt=""></li>
                            <li style="float: left; display: block; width: 114px;"><img class="img-thumbnail"
                                                                                        src="/wp-content/themes/dtbds/raw/07_home.png"
                                                                                        alt=""></li>
                            <li style="float: left; display: block; width: 114px;"><img class="img-thumbnail"
                                                                                        src="/wp-content/themes/dtbds/raw/08_home.png"
                                                                                        alt=""></li>
                        </ul>
                    </div>
                    <ol class="flex-control-nav flex-control-paging">
                        <li><a class="">1</a></li>
                        <li><a class="flex-active">2</a></li>
                    </ol>
                </div>
            </div>

            <div class="col-lg-7 col-md-7 col-sm-12">
                <h3 class="big_title">News &amp; Updates
                    <small>The most popular real estate news</small>
                </h3>
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <article class="blog-wrap">
                        <div class="blog-media">
                            <div class="fluid-width-video-wrapper" style="padding-top: 56.2%;">
                                <iframe src="/wp-content/themes/dtbds/raw/73221098.html" frameborder="0"
                                        webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""
                                        id="fitvid862923"></iframe>
                            </div>
                        </div><!-- end blog media -->
                        <div class="post-date">
                            <span class="day">01</span>
                            <span class="month">Feb</span>
                        </div><!-- end post-date -->

                        <div class="post-content">
                            <h2><a href="/">New York City 124/56 for Sale!</a></h2>
                            <p>Er zijn vele variaties van passages van Lorem Ipsum beschikbaar maar het
                                merendeel.</p>
                            <div class="post-meta">
                                <span><i class="fa fa-user"></i> <a href="/">John Doe</a> </span>
                                <span><i class="fa fa-tag"></i> <a href="/">Video</a> </span>
                                <span><i class="fa fa-comments"></i> <a href="/">1 Comments</a></span>
                            </div><!-- end post-meta -->
                            <a href="/" class="btn btn-primary btn-xs">read more</a>
                        </div><!-- post-content -->
                    </article><!-- end blog wrap -->
                </div><!-- end col-lg-6 -->

                <div class="col-lg-6 col-md-6 col-sm-12">
                    <article class="blog-wrap">
                        <div class="blog-media">
                            <div class="fluid-width-video-wrapper" style="padding-top: 56.2%;">
                                <iframe src="/wp-content/themes/dtbds/raw/64550407.html" frameborder="0"
                                        webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""
                                        id="fitvid837530"></iframe>
                            </div>
                        </div><!-- end blog media -->
                        <div class="post-date">
                            <span class="day">12</span>
                            <span class="month">Jan</span>
                        </div><!-- end post-date -->

                        <div class="post-content">
                            <h2><a href="/">Estate+ video presentation</a></h2>
                            <p>Er zijn vele variaties van passages van Lorem Ipsum beschikbaar maar het
                                merendeel.</p>
                            <div class="post-meta">
                                <span><i class="fa fa-user"></i> <a href="/">Mark Doe</a> </span>
                                <span><i class="fa fa-tag"></i> <a href="/">Video</a> </span>
                                <span><i class="fa fa-comments"></i> <a href="/">11 Comments</a></span>
                            </div><!-- end post-meta -->
                            <a href="/" class="btn btn-primary btn-xs">read more</a>
                        </div><!-- post-content -->
                    </article><!-- end blog wrap -->
                </div><!-- end col-lg-6 -->
            </div><!-- end col7 -->

        </div><!-- end row -->
    </div><!-- end dm_container -->
</section><!-- end secondwrapper -->