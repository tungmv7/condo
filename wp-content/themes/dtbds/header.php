<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

	<?php wp_head(); ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-108407182-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-108407182-1');
    </script>

</head>
<body <?php body_class(); ?>>
<div id="fb-root"></div>
<script>(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.5&appId=596806690476942";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
<script>	    
    function myFunction() {
    document.getElementById("user_login").placeholder = "Email";
}    
</script>
 
<div class="topbar clearfix">
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-xs-12">
				<div class="callus">
					<p>
						<!-- <span><i class="fa fa-envelope"></i> property.org@gmail.com</span>
						<span><i class="fa fa-phone-square"></i> (+84) 908 404 997 or ( +84) 935 44 22 90</span> -->
						<?php dynamic_sidebar('contact-info') ?>
					</p>
				</div><!-- end callus-->
			</div><!-- end col-lg-6 -->
			<div class="col-sm-4 col-xs-12 text-right">
				<div class="marketing text-right">
	                <ul class="topflags pull-right">
	                	<?php $translations = pll_the_languages(array('raw'=>1)); ?>
                		<?php foreach($translations as $translation): ?>
						<li>
							<a data-placement="bottom" data-toggle="tooltip" data-original-title="<?= $translation['name'] ?>" title="<?= $translation['name'] ?>" href="<?= $translation["url"]?>">
								<img alt="de" src="<?= $translation['flag'] ?>">
							</a>
						</li>
						<?php endforeach; ?>
	                </ul><!-- end flags -->
	                <ul class="topmenu pull-right">
	                <?php if (is_user_logged_in()) : $current_user = wp_get_current_user(); ?>  
						<li>
							<a href="<?php echo wp_logout_url(get_permalink()); ?>">
								Hi! <?php echo  $current_user->user_login ?> / Logout
							</a>
	                    </li>
					<?php else : ?>
	                    <li>
	                    	<a href="javascript:;" data-toggle="modal" data-target="#loginModal">
	                    		<i class="fa fa-lock"></i> Login / Register
                    		</a>
	                    </li>
					<?php endif;?>
	                </ul><!-- topmenu -->
	            </div><!-- end marketing -->

			</div><!-- end col-lg-6 -->
		</div><!-- end row -->
	</div><!-- end container -->
</div>

<header class="header1">
	<div class="container">
		<div class="row header-row">
			<div class="col-sm-4 col-xs-12 text-left hidden-xs">
				<div class="logo-wrapper clearfix">
					<div class="logo">
						<a href="<?= pll_home_url() ?>" title="<?= pll__("Home") ?>">
							<img src="/wp-content/themes/dtbds/images/logo-new.png">
						</a>
						<button type="button" data-toggle="collapse" data-target="#defaultmenu" class="navbar-toggle">
							<span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
						</button>
					</div><!-- /.site-name -->
				</div><!-- /.logo-wrapper -->
			</div>
			
			<div class="col-sm-8 col-xs-12">
				<?php if ( has_nav_menu( 'top_menu' )) : ?>
				<nav class="navbar navbar-default fhmm" role="navigation" style="width: 102%">
					<div class="menudrop container">
						<div class="navbar-header">
							<a class="logo-inline" href="<?= pll_home_url() ?>" title="<?= pll__("Home") ?>">
								<img src="/wp-content/themes/dtbds/images/logo-new.png">
							</a>
							<button type="button" data-toggle="collapse" data-target="#defaultmenu" class="navbar-toggle">
								<span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
							</button>
						</div>
						<?php
						wp_nav_menu(array(
								'menu' => 'top_menu',
								'theme_location' => 'top_menu',
								'depth' => 2,
								'container_id' => 'defaultmenu',
								'container' => 'div',
								'container_class' => 'collapse navbar-collapse',
								'menu_class' => 'nav navbar-nav',
								'fallback_cb' => 'wp_bootstrap_navwalker::fallback',
								'walker' => new wp_bootstrap_navwalker())
						);
						?>
					</div>
				</nav>
				<?php endif; ?>	
			</div><!-- end col-lg-6 -->

			

			
		</div><!-- end row -->

	</div><!-- end container -->

	<!-- Modal Login -->
    <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" onload="myFunction()">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Log in</h4>
                </div>
                <div class="modal-body">
                    <div class="box">
                        <?php do_action( 'wordpress_social_login' ); ?>
                        <!-- <a class="btn-face" href="#" title="">login with facebook <i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a class="btn-linkedIn" href="#" title="">login with linked in <i class="fa fa-linkedin" aria-hidden="true"></i></a> -->
                        <span class="de">OR</span>
                        <form class="frm-login" name="loginform" id="loginform" action="/wp-login.php" method="post">
                            <input class="txt-1" type="email" name="log" id="user_login" placeholder="Email">
                            <input class="txt-1" type="password" name="pwd" id="user_pass" placeholder="Password">
                            <a class="link-forgot" href="<?php echo wp_lostpassword_url(); ?>">Forgot your password?</a>
                            <!-- <input type="submit" name="wp-submit" id="wp-submit" class="button button-primary" value="Log In"> -->
                            <input type="hidden" name="redirect_to" value="/apartment-balcony-decorating-ideas-6/">
                            <button type="submit" name="wp-submit" id="wp-submit" class="btn-style-1 btn-login">Log in</button>                         
                        </form>                         
                    </div>
                    <div class="box">
                        <p>If you don't already have an account click the button bellow to create your account.</p>
                        <a class="btn-style-1 btn-create" href="/wp-login.php?action=register">Create account</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</header><!-- end header -->
