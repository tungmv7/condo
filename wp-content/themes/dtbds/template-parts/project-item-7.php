<li>
    <div class="ps-desc">
        <h3><a href="javascript:;" title="<?= the_title_attribute() ?>"><?= get_the_title() ?></a></h3>
        <h4><?= get_the_excerpt() ?></h4>
    </div>
    <a href="javascript:;"><img src="<?= get_the_post_thumbnail_url(get_the_ID(), 'large') ?>" alt="" style="height: 35em;"></a>
</li>