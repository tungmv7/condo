<script>
    function chatWithUs() {
        $crisp.push(["do", "chat:open"]);
        $crisp.push(["do", "message:show", ["text", "Can I help?"]]);
    }
</script>
<div class="widget clearfix contact-box">
    <h3>We're here to help</h3>
    <p>Need help? Connect with one our experts. We look forward to helping you with you real estate needs.</p>
    <a href='javascript:chatWithUs();' class="btn-chat-with-us">Chat With Us</a>
</div>