<div class="col-lg-4 col-md-4 col-sm-12">
    <div class="grid">
        <div class="grid-item">
            <article class="art-card">
               <?php if ($thumbnail = get_the_post_thumbnail_url(get_the_ID(), "thumbnail")): ?>
                <div class="thumbs">
                    <img style="width: 100%;" src="<?= $thumbnail ?>" alt="">
                    <a class="layer-link" href="<?= get_permalink() ?>"><span>Read more</span></a>
                </div>
               <?php else: ?>
                   <div class="thumbs">
                       <img style="100%" src="/wp-content/themes/dtbds/images/default-thumb.png" />
                       <a class="layer-link" href="<?= get_permalink() ?>"><span>Read more</span></a>
                   </div>
                <?php endif; ?>
            <div class="inner">
                <h2><a href="<?= get_permalink() ?>"><?= get_the_title() ?></a></h2>
                <p><?= wp_trim_words(get_the_content(), 30) ?></p>
                <a class="btn-more" href="<?= get_permalink() ?>" title="" >Read more</a>
                <ul class="list-time">
                    <li><?= get_the_date('M') ?> <span><?= get_the_date('d') ?></span>, <?= get_the_date('Y') ?></li> /
                    <li><a href="#" title=""><?= wp_count_comments(get_the_ID())->approved ?> comment</a></li>
                </ul>
            </div>
            </article>
        </div>

    </div>
</div>                        